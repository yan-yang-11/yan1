//route 访问路径，路由
//router 路由管理者
import { createRouter, createWebHashHistory } from 'vue-router';// 创建路由实例并传递 `routes` 配置const router = createRouter({// 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
//创建路由 route

import index  from  "@/views/index.vue"
import login  from  "@/views/login.vue"
import register  from  "@/views/register.vue"
import user  from  "@/views/users/user.vue"
import forget  from  "@/views/forget.vue"
import articleInfo from "@/views/articleInfo.vue"

const routes = [
   { path: '/', component: index },
   { path: '/login', component: login },
   { path: '/register', component: register },
   { path: '/user', component: user},
   { path: '/forget', component: forget},
   { path: '/articleInfo',component: articleInfo},

]
//创建路由管理 router
const router = createRouter({// 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes,}
  );
 //导出路由
export default router