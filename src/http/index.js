// src/http/index.js
//创建统一请求api
import axios from './axios'
/**
  @author ***
  @params data 请求参数 json 格式
  @description 用户登录方法
*/
const login=(data)=>{
    return axios.request({
        url:'/api/mobile/elogin',//api/mobile/elogin
        method:'post',
        data
    })
}
const register = (data)=>{
    return axios.request({
        url:'/api/mobile/eregister',
        method:'post',
        data
    })
}
//获取主页数据
const getAllArticle = ()=>{
    return axios.request({
        url:'/api/article/all',
        method:'get',
    })
}

const getUserInfo = (data)=>{
    return axios.request({
        url:'/api/userinfo/one',
        method:'get',
        params:data
    })
}
const getActicleCat = (data)=>{
    return axios.request({
        url:'/api/articleCat/one',
        method:'get',
        params:data
    })
}
const getMyArticle = (data)=>{
    return axios.request({
        url:`/api/article/getArticles/${data.userId}`,
        method:'get',       
    })
}

//导出请求方法
export {
    login,
    register,
    getAllArticle,
    getUserInfo,
    getActicleCat,
    getMyArticle
}