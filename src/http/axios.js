//引入axios
import axios from "axios";
import {useCounterStore} from '@/store/counter'

//创建axios实例
const instance = axios.create({
    baseURL: 'https://api.jqrjq.cn',//请求基地址
    timeout: 6000
})
// 请求拦截器 发起请求时设置token
instance.interceptors.request.use((config) => {
    const useToken = useCounterStore()
    const token = useToken.getToken;
    if(token != ''){
        config.headers['token'] = token
    }
    return config;
}, (err) => {
    return Promise.reject(err)
})
//响应拦截器
instance.interceptors.response.use((res) => {
    return res
}, (err) => {
    return Promise.reject(err)
})
export default instance;