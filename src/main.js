//main.js
import 'element-plus/dist/index.css' //引入element-plus 样式
import router from './router'
import {createPinia} from 'pinia'
import { createApp } from 'vue'
import App from './App.vue'
import {useCounterStore} from '@/store/counter'


const pinia=createPinia()

const app=createApp(App)
app.use(router)
app.use(pinia)

app.mount('#app')
const useCounter = useCounterStore()
router.beforeEach((to,from) =>{
    const url = to.path
    const token = useCounter.getToken
    if(url == '/login' || url == '/register'){
        if(token != '' && url != '/register'){
            router.replace({path:'/'})
        } else {
            return true
        }
    }
    if(token == '' && (url != '/register' || url != '/login')){
       return '/login'
    }
    return true
})
